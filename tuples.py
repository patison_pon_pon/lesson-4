# Tuples - кортежи. Это Практически то же самое что list (список), но неизменяемые (imutable).
# То есть мы их не можем изменять

tuple_1 = (1, 2, 3)
tuple_2 = ('one', 'hello', 'world')
tuple_3 = (3, 2.3, 'three')

print(tuple_1)
print(tuple_2)
print(tuple_3)

# если в кортеже только один эелемент, то надо оставлять после него запятую
one_element_tuple = (1, )
print(one_element_tuple)

# tuple можно создать и без скобок. Скобки нужны для наглядности и понятности. Их нужно использовать всегда
tuple_1 = 1, 2, 3
print(tuple_1)

# tuple неизменяемые (imutable). Мы ничего в них не можем изменять
# tuple_1[2] = 4 # это будет ошибка

new_tuple = (tuple_1[0], 100500, tuple_1[2])
print(new_tuple)

# обращаться по индексу можно так же как в листах
print(tuple_1[1])
print(tuple_1[-1])


person_tuple = ('Петя', 'Копченный', 2001)
print(person_tuple)
# распаковка в переменные
x = y = z = 12
print(x, y, z)
x, y, z = 12, 13, 14
print(x, y, z)
first_name, last_name, year_of_birth = person_tuple
print(first_name, last_name, year_of_birth)

# считаем к-во эелементов в кортеже
t1 = (1, 2, 5, 1, 7, -100500, 'hello', 2, 1)
print(t1.count(1))
print(t1.count(3))

# Создаем кортеж/список с строки
tuple_from_str = tuple("Как же это все скучно")
list_from_str = list("Как же это все скучно")

# находим элемент в кортеже/списке
print(2 in t1)
print(6 in t1)
print("ж" in tuple_from_str)
print("ж" in list_from_str)
print("w" in list_from_str)

# узнать индекс элемента
print(t1.index(5))
letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
print(letters)
print(list(letters))
