# Словари - структуры, которые содержат неупорядочные эелементы.
# Держатся в парах: ключ - значение. Ключ - строка или другой неизменяемый обьект
car_prices = {"opel": 5000, "toyota": 7000, 'bmw': 10000}
print(car_prices)

# добавление
print(car_prices['toyota'])
car_prices['mazda'] = 4000
print(car_prices)
# не могут иметь одинаковых ключей. Ключи уникальны
car_prices['opel'] = 2000
print(car_prices)

# удаление
del car_prices['toyota']
print(car_prices)

# удаление всего словаря
# del car_prices
# print(car_prices)

# полная очистка словаря
# car_prices.clear()
# print(car_prices)

person = {
    'first_name': 'Валера',
    'last_name': 'Косой',
    'age': 42,
    'character': 'Мерзкий',
    'hobbies': ['футбол', 'пиво после работы', 'ругать политиков'],
    'children': {
        "son": {
            'first_name': 'Гаврила',
            'last_name': 'Косой',
            'age': 16,
            'character': 'Скверный',
            'hobbies': ['пить pepsi за гаражами', 'трясти мелочуху с младших', 'дротить в RAID SL'],
        },
        "daughter": {
            'first_name': 'Жозефина',
            'last_name': 'Косая',
            'age': 14,
            'character': 'Отсутствует',
            'hobbies': ['снимать бьюти-блог', 'обижаться'],
        },
    }
}

print(person['age'])
print(person['hobbies'])
hobbies = person['hobbies']
print(hobbies[2])
print(person['hobbies'][2])

children = person['children']
print(children['daughter'])
print(person['children']['daughter'])

person['car'] = 'Таврия'
# получить ключи
print(person.keys())
print(type(person.keys()))
print(list(person.keys()))
# получить значения
print(person.values())
# получить ключи-значения
print(person.items())

# Пустой dict всегда работает как False
print(bool(dict()))
empty_dict = {}
if empty_dict:
    print('True')
else:
    print('False')

empty_dict['key'] = 'value'
if empty_dict:
    print('True')
else:
    print('False')
