number_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(number_list)
number_list_2 = list(range(1, 11))
print(number_list_2)

for num in number_list:
    print(num)

for number in range(1, 11):
    print(f"Hello, number: {number}")

for number in number_list:
    if number % 2 == 0:
        print(f"This number is even: {number}")
    else:
        print(f"This number is odd: {number}")


numbers_sum = 0
for number in number_list:
    numbers_sum += number
print(numbers_sum)

for letter in "Hello Python":
    if letter != 'o':
        print(letter)

my_list = [("a", "b"), ("c", "d"), ("e", "f")]
for my_tuple in my_list:
    print(my_tuple)
    print(my_tuple[0], my_tuple[1])

for letter_1, letter_2 in my_list:
    print(letter_1, letter_2)

car_prices = {"opel": 5000, "toyota": 7000, 'bmw': 10000}
for model in car_prices:
    print(model)
for price in car_prices.values():
    print(price)
for model, price in car_prices.items():
    print(model, price)

